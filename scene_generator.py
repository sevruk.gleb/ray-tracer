from hits import sphere
from materials import lambertian, metal, dielectric
from utils import drand48
from vec import vec3


def generate_scene():
    n = 500
    list = []
    list.append(sphere(vec3(0, -1000, 0), 1000.0, lambertian(vec3(0.5, 0.5, 0.5))))
    i = 1
    for a in range(-11, 11, 1):
        for b in range(-11, 11, 1):
            choose_mat = drand48()
            center = vec3(a + 0.9 + drand48(), 0.2, b + 0.9 + drand48())
            if (center - vec3(4, 0.2, 0)).norm() > 0.9:
                if choose_mat < 0.8:
                    # difuse
                    list.append(sphere(center, 0.2, lambertian(vec3(drand48() * drand48(), drand48() * drand48(), drand48() * drand48()))))
                elif choose_mat < 0.95:
                    #metal
                    list.append(sphere(center, 0.2, metal(
                        vec3(
                            0.5*(1 + drand48()),
                            0.5*(1 + drand48()),
                            0.5*(1 + drand48())),
                        0.5*(1 + drand48()))))
                else:
                    # glass
                    list.append(sphere(center, 0.2, dielectric(1.5)))
    list.append(sphere(vec3(0, 1, 0), 1.0, dielectric(1.5)))
    list.append(sphere(vec3(-4, 1, 0), 1.0, lambertian(vec3(0.4, 0.2, 0.1))))
    list.append(sphere(vec3(4, 1, 0), 1.0, metal(vec3(0.7, 0.6, 0.5), 0.0)))
    return list

