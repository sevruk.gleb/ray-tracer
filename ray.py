from vec import vec3


class Ray:
    def __init__(self, a: vec3, b: vec3):
        self.origin = a
        self.direction = b
        pass

    def point_at_parameter(self, t: float):
        return self.origin + self.direction * t

