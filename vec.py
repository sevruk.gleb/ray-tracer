import math

class vec3:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def dot(self, v2) -> float:
        return self.x * v2.x + self.y * v2.y + self.z * v2.z

    def sqr_mag(self):
        return self.dot(self)

    def cross(self, v2):
        x = self.y * v2.z - self.z * v2.y
        y = self.x * v2.x - self.x * v2.z
        z = self.z * v2.y - self.y * v2.x
        return vec3(x, y, z)

    def normalize(self):
        return self.__div__(self.norm())

    def norm(self):
        return math.sqrt(vec3.dot(self, self))

    def __add__(self, v):
        return vec3(self.x + v.x, self.y + v.y, self.z + v.z)

    def __neg__(self):
        return vec3(-self.x, -self.y, -self.z)

    def __sub__(self, v):
        return self + (-v)

    def __mul__(self, v):
        if isinstance(v, vec3):
            return vec3(self.x * v.x, self.y * v.y, self.z * v.z)
        else:
            return vec3(self.x * v, self.y * v, self.z * v)

    def __rmul__(self, v):
        return self.__mul__(v)

    def __div__(self, v):
        if isinstance(v, vec3):
            return vec3(self.x / v.x, self.y / v.y, self.z / v.z)
        else:
            return vec3(self.x / v, self.y / v, self.z / v)

    def __str__(self):
        return '[ %.4f, %.4f, %.4f ]' % (self.x, self.y, self.z)