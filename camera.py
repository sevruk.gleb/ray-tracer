import math

from ray import Ray
from utils import unit_vector, drand48
from vec import vec3


def random_in_unit_disk() -> vec3:
    while True:
        p = 2.0*vec3(drand48(),drand48(),0) - vec3(1,1,0)
        if vec3.dot(p,p) < 1.0:
            break
    return p

class camera:
    def __init__(self, lookfrom: vec3, lookat: vec3, vup: vec3, vfov: float, aspect: float, aperture : float, focus_dist : float):
        self.lens_radius = aperture / 2
        theta = vfov * math.pi / 180
        half_height = math.tan(theta / 2)
        half_width = aspect * half_height
        self.origin = lookfrom
        self.w = unit_vector(lookfrom - lookat)
        self.u = unit_vector(vec3.cross(vup, self.w))
        self.v = vec3.cross(self.w, self.u)
        self.lower_left_corner = self.origin - half_width*focus_dist*self.u -half_height*focus_dist*self.v -focus_dist * self.w
        self.horizontal = 2 * half_width * focus_dist* self.u
        self.vertical = 2 * half_height * focus_dist* self.v

    def get_ray(self, s: float, t: float):
        rd = self.lens_radius*random_in_unit_disk()
        offset = (self.u * rd.x) + (self.v * rd.y)
        return Ray(self.origin + offset, self.lower_left_corner + s * self.horizontal + t * self.vertical - self.origin - offset)
