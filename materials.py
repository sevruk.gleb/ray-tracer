from math import sqrt
from typing import Tuple

from utils import random_in_unit_sphere, unit_vector, drand48
from hits import hit_record
from ray import Ray
from vec import vec3


class scatter_result:
    def __init__(self, state: bool, scattered: Ray, attenuation: vec3):
        self.state = state
        self.scattered = scattered
        self.attenuation = attenuation


class material:
    # разброс
    def scatter(self, r_in: Ray, rec: hit_record) -> scatter_result:
        pass


def reflect(v : vec3, n: vec3):
    return v - 2 * v.dot(n) * n



def refract(v : vec3, n: vec3, ni_over_nt: float) -> Tuple[bool, vec3]:
    # returns refracted
    refracted = None
    uv = unit_vector(v)
    dt = vec3.dot(uv, n)
    discriminant = 1.0 - ni_over_nt*ni_over_nt*(1-dt*dt)
    if (discriminant > 0):
        refracted = ni_over_nt * (v - n*dt) - n*sqrt(discriminant)

    return refracted is not None, refracted


def schlick(cosine : float, ref_idx : float) -> float:
    r0 = (1-ref_idx) / (1+ref_idx)
    r0= r0*r0
    return r0 + (1-r0)*pow((1- cosine), 5)

class lambertian(material):
    def __init__(self, a: vec3):
        self.albedo = a

    def scatter(self, r_in: Ray, rec: hit_record):
        target = rec.p + rec.normal + random_in_unit_sphere()
        return scatter_result(True, Ray(rec.p, target - rec.p), self.albedo)


class metal(material):
    def __init__(self, a: vec3, fuzz: float):
        self.fuzz = fuzz if fuzz < 1 else 1
        self.albedo = a

    def scatter(self, r_in: Ray, rec: hit_record):
        reflected = reflect(unit_vector(r_in.direction), rec.normal)
        scattered = Ray(rec.p, reflected +  self.fuzz*random_in_unit_sphere())
        # scattered = Ray(rec.p, reflected)
        should_reflect = scattered.direction.dot(rec.normal) > 0
        return scatter_result(should_reflect, scattered, self.albedo)
    
class dielectric(material):
    def __init__(self, ref_idx):
        self.ref_idx = ref_idx

    def scatter(self, r_in: Ray, rec: hit_record) -> scatter_result:
        outward_normal = None
        reflected = reflect(r_in.direction, rec.normal)
        ni_over_nt = 0.0
        attenuation = vec3(1.0,1.0,0.0)
        refracted = vec3(0,0,0)
        reflect_prob = 0.0
        cosine = 0.0
        if vec3.dot(r_in.direction, rec.normal) > 0:
            outward_normal = -rec.normal
            ni_over_nt = self.ref_idx
            cosine = self.ref_idx * vec3.dot(r_in.direction, rec.normal) / r_in.direction.norm()
        else:
            outward_normal = rec.normal
            ni_over_nt = 1.0 / self.ref_idx
            cosine = -vec3.dot(r_in.direction, rec.normal) / r_in.direction.norm()


        scattered = None
        is_refracted, refracted = refract(r_in.direction, outward_normal, ni_over_nt)
        if is_refracted:
            reflect_prob = schlick(cosine, self.ref_idx)
        else:
            scattered = Ray(rec.p, reflected)
            reflect_prob = 1.0

        if drand48() < reflect_prob:
            scattered = Ray(rec.p, reflected)
        else:
            scattered = Ray(rec.p, refracted)

        return scatter_result(True, scattered, attenuation)

