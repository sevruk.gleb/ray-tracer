import sys
import threading
import tkinter as tk
from math import sqrt

import PIL as pil
from PIL import Image, ImageTk

from camera import camera
from hits import Hitable, sphere, hitable_list
from materials import lambertian, metal, dielectric
from ray import Ray
from scene_generator import generate_scene
from utils import drand48, unit_vector
from vec import vec3
from timeit import default_timer as timer

global_image = None
should_halt = False

all_images = [
    '/home/gleb/Pictures/panhadne_icon.png',
    '/home/gleb/Pictures/1.png',

]


def fuuu():
    should_halt = True
    root.destroy()
    pass


def load_pil_image_from_disk(file_name) -> Image:
    im = Image.open(file_name)
    im = im.resize((600, 600))
    return im


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.label = tk.Label()
        # self.label.image   # keep a reference!
        self.label.pack()
        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=fuuu)
        self.quit.pack(side="bottom")

    def say_hi(self):
        print("hi there, everyone!")


root = tk.Tk()
app = Application(master=root)


def hit_sphere(center: vec3, radius: float, r: Ray):
    oc = r.origin - center
    a = r.direction.dot(r.direction)
    b = 2.0 * oc.dot(r.direction)
    c = oc.dot(oc) - radius * radius
    discriminant = b * b - 4 * a * c
    if discriminant < 0:
        return -1.0
    else:
        return (-b - sqrt(discriminant)) / (2.0 * a)


def color(r: Ray, world: Hitable, depth: int) -> vec3:
    has_hit, rec = world.hit(r, 0.001, sys.float_info.max)
    if has_hit:
        if depth < 50:
            s_res = rec.material.scatter(r, rec)
            if s_res.state:
                # if depth > 2:
                #     print(depth)
                return s_res.attenuation * color(s_res.scattered, world, depth + 1)
        return vec3(0, 0, 0)
        # target = rec.p + rec.normal + random_in_unit_sphere()
        # # return 0.5 *  vec3(rec.normal.x + 1, rec.normal.y + 1, rec.normal.z + 1)
        # return 0.5 * color(Ray(rec.p, target - rec.p), world=world)
    else:
        unit_direction = unit_vector(r.direction)
        t = 0.5 * (unit_direction.y + 1.0)
        return (1.0 - t) * vec3(1.0, 1.0, 1.0) + t * vec3(0.5, 0.7, 1.0)


def render_loop():
    print('==== Thread Start')
    counter = 1
    while not should_halt:
        print(f'Frame: {counter}')

        # time.sleep(0.01)

        scale = 40
        nx = 20 * scale
        ny = 10 * scale

        # AA factor/ multipass
        ns = 2
        frame_buffer = pil.Image.new(mode='RGB', size=(nx, ny))

        # replace_image(frame_buffer)

        # scene = demo_scene()

        scene = generate_scene()
        world = hitable_list(scene)
        lookfrom = vec3(13, 2, 3)
        lookat = vec3(0, 0, 0)
        dist_to_focus = 10.0
        aperture = 0.01
        cam = camera(lookfrom, lookat, vec3(0, 1, 0), 20, float(nx)/ float(ny), aperture, dist_to_focus)
        start = timer()

        for i in range(0, nx):
            for j in range(0, ny):
                col = vec3(0, 0, 0)
                for s in range(ns):
                    u = float(i + drand48()) / nx
                    v = float(j + drand48()) / ny
                    r = cam.get_ray(u, v)
                    p = r.point_at_parameter(2.0)
                    col += color(r=r, world=world, depth=0)

                col = col.__div__(ns)
                col = vec3(sqrt(col.x), sqrt(col.y), sqrt(col.z))
                ir = int(255.99 * col.x)
                ig = int(255.99 * col.y)
                ib = int(255.99 * col.z)
                target_y = ny - 1 - j
                frame_buffer.putpixel(xy=(i, target_y), value=(ir, ig, ib))
            if counter <= 1:
                replace_image(frame_buffer)
                print(f'hline {i} of {nx}')

        end = timer()
        frame_time = int((end - start) * 1000)
        if counter > 1:
            replace_image(frame_buffer)

        counter += 1
        print(f'{frame_time}ms')

    # app.label.pack()
    print('==== END thread')


def demo_scene():
    scene = []
    scene.append(sphere(vec3(0, 0, -1), radius=0.5, material=lambertian(vec3(0.1, 0.2, 0.5))))
    scene.append(sphere(vec3(0, -100.5, -1), 100.0, lambertian(vec3(0.8, 0.8, 0.0))))
    scene.append(sphere(vec3(1, 0, -1), 0.5, metal(vec3(0.8, 0.6, 0.2), fuzz=0.1)))
    # scene.append(sphere(vec3(-1, 0, -1), 0.5, metal(vec3(0.8, 0.6, 0.2),fuzz=0.1)))
    scene.append(sphere(vec3(-1, 0, -1), 0.5, dielectric(1.6)))
    scene.append(sphere(vec3(-1, 0, -1), -0.45, dielectric(1.6)))
    return scene


def replace_image_from_file(file_name):
    photo = load_pil_image_from_disk(file_name)
    replace_image(photo)


def replace_image(pil_image):
    pil_image = ImageTk.PhotoImage(pil_image)
    app.label.configure(image=pil_image)
    # app.label(image=photo)
    app.label.image = pil_image


render_thread = threading.Thread(target=render_loop)
render_thread.start()

app.mainloop()
