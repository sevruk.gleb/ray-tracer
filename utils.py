from random import random

from vec import vec3


def drand48():
    return random()


def random_in_unit_sphere():
    p = None
    while True:
        p = 2.0 * vec3(drand48(), drand48(), drand48()) - vec3(1, 1, 1)
        if p.dot(p) < 1.0:
            break
    return p


def unit_vector(v: vec3) -> vec3:
    return v.normalize()