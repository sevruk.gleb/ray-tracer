from math import sqrt
from typing import Tuple

from ray import Ray
from vec import vec3


if False:
    import materials

class hit_record:
    def __init__(self, t=None, p=None, normal=None, _material: 'materials.material' = None):
        self.t = t
        self.p = p
        self.normal = normal
        self.material = _material

class Hitable:
    def hit(self, ray: Ray, t_min: float, t_max: float)-> Tuple[bool, hit_record]:
        pass


class sphere(Hitable):
    def __init__(self, center : vec3, radius: float, material: 'materials.material'):
        self.material = material
        self.center = center
        self.radius = radius

    def hit(self, ray: Ray, t_min: float, t_max: float) -> Tuple[bool, hit_record]:
        oc = ray.origin - self.center
        rec = None
        a = ray.direction.dot(ray.direction)
        b = oc.dot(ray.direction)
        c = oc.dot(oc) - self.radius * self.radius
        discriminant = b * b - a * c

        if discriminant >= 0:
            temp = (-b - sqrt(b*b - a*c)) / a
            rec = self.getHitIfPresent(ray, rec, t_max, t_min, temp)
            if(rec is not None):
                return True, rec
            temp = (-b + sqrt(b * b - a * c)) / a
            rec = self.getHitIfPresent(ray, rec, t_max, t_min, temp)

        return rec is not None, rec

    def getHitIfPresent(self, ray, rec, t_max, t_min, temp):
        if t_max > temp > t_min:
            p = ray.point_at_parameter(temp)
            # center_ = (p - self.center).normalize()
            rec = hit_record(temp, p, (p - self.center).__div__(self.radius), self.material)
        return rec


class hitable_list(Hitable):



    def __init__(self, scene_objects):
        self.scene_objects = scene_objects


    def hit(self, ray: Ray, t_min: float, t_max: float) -> Tuple[bool, hit_record]:
        hit_rec = None
        hit_anything = False
        closest_so_far = t_max
        for index, obj in enumerate(self.scene_objects):
            has_hit, hit_obj = obj.hit(ray, t_min, closest_so_far)
            if has_hit:
                hit_anything = True
                closest_so_far = hit_obj.t
                hit_rec = hit_obj

        return hit_anything, hit_rec
